import { Controller, Post, Res, Body, Get, Delete, Patch, Options } from '@nestjs/common';
import { PollService } from './poll.service';

@Controller('poll')
export class PollController {

    constructor(private pollService: PollService) {}

    @Post()
    async submitVote(@Body() poll: string) {
        return await this.pollService.create(poll);
    }

}
