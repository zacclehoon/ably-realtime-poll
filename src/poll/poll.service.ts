import { Injectable } from '@nestjs/common';
import * as Ably from 'ably';
const ably = new Ably.Realtime('cYuzAQ.1pzz9Q:SfbcBs6ENfJlQH7c');

@Injectable()
export class PollService {

    async create(poll: any): Promise<void> {
        const channel = ably.channels.get('ably-nest');
        const data = {
            points: 1,
            movie: poll.movie,
        };
        return await channel.publish('vote', data);
    }

}
